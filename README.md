# TP_MAPD



## Petri Net 
A Petri net is a mathematical model used to represent various types of systems, such as computing and industrial systems. It enables the modeling and verification of the dynamic behavior of discrete-event systems. A Petri net consists of places, which can contain tokens, and transitions. Arcs (carrying a specific value) link places to transitions (outgoing arcs from places) and transitions to places (incoming arcs into places). If unspecified, the value of an arc defaults to 1.

A transition is enabled when the places connected to it by an incoming arc have at least as many tokens as indicated by the arc’s value. When an enabled transition fires (called a "step"), tokens are removed from the upstream places according to the values of the outgoing arcs, and tokens are added to the downstream places based on the values of the incoming arcs. Therefore, the total number of tokens can change with each step.

The goal of this project is to design and develop a tool to simulate a Petri net.



## Quick Start

Now let's simulate a Petri Net, first clone the project from GitLab using this command: 

`git clone https://gitlab.imt-atlantique.fr/s23lachg/tp_mapd`

Import the project on your IDE's workspace, make sure you add JUnit to your build path to avoid getting error messages, since it is used to run tests for the projects.

Now you can simulate your Petri Net, to do so open the petrinet package and run the main file, if you are more curious and want to create your own Petri Net, we created an "InteractiveMain" file, it'll allow you to chose the number of tokens, the arcs weights using the console so you can get an in-depth understanding of the functioning of a Petri Net.  

You can see that places, transitions, arcs and tokens are already created and their values are set by default, if you want to change their values and try a simulation with a different number of elements, you can do so by editing the class main. 


## Development

This project is developed by Soufiane LACHGUER and Mouad SOUISSI, for a university project related to petri nets. It uses Java 23.0.1 and JUnit 5. Files are shared via Gitlab.

If you encounter any technical problems regarding this project, you can write to us at soufiane.lachguer@imt-atlantique.net or at mouad.souissi@imt-atlantique.net

## Differences with the initial conception 

This image shows the class diagramm obtained at the development stage : 

![Class diagramm](images/diagramm_image.png)

One of the main differences is regarding the Arc class. In the implementation, it is defined as an abstract class because the executemove() method had different usage cases depending on whether the arc is exiting, emptying, or entering arc. In the conception diagramm, these type cases are treated via a String type attribute. We opted to remove this attribute to use the inheritance more effectively, and to avoid inconsistencies in the code. 

The other main difference is the removal of the fire(Transition) method present in the PetriNet and IPetrinet classes. To execute transitions in the petri net, we use the run() methods which fires all the fireable transitions (checked via the isFeasible() method in transition, and fired via the execute() method in the Transition class). The treatment of the tokens is handled by the executemove() method in the Arc class and its sub-classes (calling the adders and removers of the attribute token in the Place class if necessary)

Some minor changes include the addition of 2 methods in the PetriNet class (checkForDuplicateArcs() and displayPetriNetDetails()). checkForDuplicateArcs() allows us to see whether some arcs are entering (or exiting) the same place via (or to) the same transition. Also, getters, setters, adders and removers not present in the conception diagramm were added in the implementation. We changed some names between the conception and implementation diagramm classes, to respect naming conventions. Some notable namechanges include : jeton to tokens (in all iterations of the word jeton), isMovePossible() to isFeasible(), ArcEntrant to EnArc, ArcSortant to ExArc, ArcZero to ZArc, ArcVideur to EmArc, poids to weight (in all iterations of the word poids), and fire() to execute().

